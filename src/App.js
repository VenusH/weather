import React, { useState } from 'react';
import axios from "axios";
import Conntent from './compponents/Content';
import Form from './compponents/Form';
import { Alert } from 'reactstrap';
import "weather-icons/css/weather-icons.css";

const API_KEY = "ee5c3b26cced431451f79e798792c4d7";

const App = () => {
  const [error, setError] = useState("");
  const [icon, setIcon] = useState(undefined);
  const [items, setItems] = useState([]);

  const weatherIcon = {
    Thunderstorm: "wi-thunderstorm",
    Drizzle: "wi-sleet",
    Rain: "wi-storm-showers",
    Snow: "wi-snow",
    Atmosphere: "wi-fog",
    Clear: "wi-day-sunny",
    Clouds: "wi-day-fog"
  };

  const get_WeatherIcon = (rangeId) => {
    if (rangeId >= 200 && rangeId < 232) {
      setIcon(weatherIcon.Thunderstorm);
    } else if (rangeId >= 300 && rangeId <= 321) {
      setIcon(weatherIcon.Drizzle);
    } else if (rangeId >= 500 && rangeId <= 521) {
      setIcon(weatherIcon.Rain);
    } else if (rangeId >= 600 && rangeId <= 622) {
      setIcon(weatherIcon.Snow);
    } else if (rangeId >= 701 && rangeId <= 781) {
      setIcon(weatherIcon.Atmosphere);
    } else if (rangeId === 800) {
      setIcon(weatherIcon.Clear);
    } else if (rangeId >= 801 && rangeId <= 804) {
      setIcon(weatherIcon.Clouds);
    } else {
      setIcon(weatherIcon.Clouds);
    }
  }

  const handleDelete = itemId => {
    setItems(items.filter(item => item.id !== itemId));
  };



  const locationWeather = async (e) => {
    e.preventDefault();
    const city = e.target.elements.city.value;
    const country = e.target.elements.country.value;

    const api = await axios.get(`http://api.openweathermap.org/data/2.5/weather?q=${city},${country}&appid=${API_KEY}&units=metric`);
    const response = await api.data;

    get_WeatherIcon(response.weather[0].id);

    if ((city && country) && icon !== undefined) {
      setItems([...items, {
        id: items.length,
        city: response.name,
        temperature: response.main.temp,
        country: response.sys.country,
        humidity: response.main.humidity,
        description: response.weather[0].description,
        icon: icon
      }
      ]);

    } else {
      setError({ error: 'Please Enter a Value' });

    }

  }

  return (
    <div className="App">
      <Conntent />
      <Form locationWeather={locationWeather} />
      <div className="row" style={{ padding: 10 }}>
        {
          items.map(item => (
            <div className="col-4" style={{ paddingRight: '10px' }}>
              <div style={{ borderWidth: '10px', backgroundColor: '#f5f5f5', padding: '10px' }}>
                <h1>{item.country}</h1>
                <p>City: {item.city}</p>
                <i className={`wi ${item.icon} display-1`} />

                <p>Temperature: {item.temperature} °C</p>
                <button onClick={() => handleDelete(item.id)}>Remove</button>
                <p> {item.error}</p>

              </div>
            </div>
          ))
        }
      </div>
    </div >

  );

}

export default App;
