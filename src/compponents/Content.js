import React from 'react';
import { render } from '@testing-library/react';


class Content extends React.Component {
    render() {
        return (
            <div>
                <h1>Weather</h1>
                <p>Find out temperature</p>
            </div>

        );
    }
}

export default Content;
